import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  openDraw: {
    height: 40,
    width: 40,
  },
  flex2: {
    flex: 2,
  },
  flex3: {
    flex: 3,
  },
  toolbarTitle: {
    color: 'black',
    fontSize: 26,
  },
  drawerButton: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    paddingTop: 2,
  },
  toolbar: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
});

export default styles;
